describe('Test Bathymetric Survey - 1992-04-9 - Asamant Shoal article ', function () {
    beforeEach(function () {
        cy.visit('https://dev.thedatatimes.com/dataset/BFSqO20Bmah2eTSC6n7h')           
    })
    it('How it works', function () {
        cy.contains('How it works').click({force: true})   
    })   
    it('Imagine', function () {       
        cy.contains('Data Times').click({force: true})
            cy.url()
                .should('include', '/')           
    })
    it('About', function () {       
        cy.contains('About').click({force: true})
            cy.url()
                .should('include', '/about')           
    })
    it('Support', function () {       
    cy.contains('Support').click({force: true})
        cy.url()
            .should('include', '/#the-data-times')           
    })
    it('Contact', function () {
        cy.contains('Contact')
    })
    it('0.2', function () {
        cy.contains('0.2')
    })
    it('Input-bar', function () {
        cy.get('[type="text"]').clear()
    })
    it('Search Data Times', function () {
        cy.contains('Search Data Times').click({force: true}) 
    }) 
    it('?', function () {
        cy.get('[role = "img"]')
    })
    it('Back to Search Results', function () {
        cy.contains('Back to Search Results').click({force: true}) 
        cy.url()
    })
    
    describe('Topics', function () {
        it('water', function () {
            cy.contains('water').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=water')
        })
        it('volunteers', function () {
            cy.contains('volunteers').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=volunteers')
        })
        it('open source', function () {
            cy.contains('open source').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=open%20source')
        })
        it('communities', function () {
            cy.contains('communities').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=communities')
        })
        it('sand', function () {
            cy.contains('sand').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=sand')
        })
        it('health', function () {
            cy.contains('health').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=health')
        })
        it('sport', function () {
            cy.contains('sport').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=sport')
        })
        it('gps', function () {
            cy.contains('gps').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=gps')
        })
        it('technology', function () {
            cy.contains('technology').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=technology')
        })
        it('adventure', function () {
            cy.contains('adventure').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=adventure')
        })
    })
    it('lintol', function () {
        cy.contains('Lintol')
    })

})


