describe('Test Guatemala- 3W page ', function () {
    beforeEach(function () {
        cy.visit('https://dev.thedatatimes.com/dataset/R52PX20BdeI7J_hLmLJr')           
    })
    it('How it works', function () {
        cy.contains('How it works').click({force: true})   
    })   
    it('Imagine', function () {       
        cy.contains('Data Times').click({force: true})
            cy.url()
                .should('include', '/')           
    })
    it('About', function () {       
        cy.contains('About').click({force: true})
            cy.url()
                .should('include', '/about')           
    })
    it('Support', function () {       
    cy.contains('Support').click({force: true})
        cy.url()
            .should('include', '/#the-data-times')           
    })
    it('Contact', function () {
        cy.contains('Contact')
    })
    it('0.2', function () {
        cy.contains('0.2')
    })
    it('Input-bar', function () {
        cy.get('[type="text"]').clear()
    })
    it('Search Data Times', function () {
        cy.contains('Search Data Times').click({force: true}) 
    }) 
    it('?', function () {
        cy.get('[role = "img"]')
    })
    it('Back to Search Results', function () {
        cy.contains('Back to Search Results').click({force: true}) 
        cy.url()
    })
    
    describe('Topics', function () {
        it('(need to add all gov departments).', function () {
            cy.contains('(need to add all gov departments).').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=%28need%20to%20add%20all%20gov%20departments%29.')
        })
        it('waiting times', function () {
            cy.contains('waiting times').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=waiting%20times')
        })
        it('doctors', function () {
            cy.contains('doctors').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=doctors')
        }) 
        it('brexit', function () {
            cy.contains('brexit').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=brexit')
        }) 
        it('trafficking', function () {
            cy.contains('trafficking').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=trafficking')
        }) 
        it('crime', function () {
            cy.contains('crime').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=crime')
        })
        it('ombudsman', function () {
            cy.contains('ombudsman').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=ombudsman')
        })
        it('politics', function () {
            cy.contains('politics').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=politics')
        })
        it('health', function () {
            cy.contains('health').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=health')
        })
    }) 
    it('Using This Dataset ', function () {
        cy.contains('Using This Dataset').click({force: true})   
    }) 
    it('lintol', function () {
        cy.contains('Lintol')
    })
    
})