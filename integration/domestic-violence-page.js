describe('Test domestic violence page ', function () {
    beforeEach(function () {
        cy.visit('https://dev.thedatatimes.com/searchresults?cats=domestic%20violence')           
    })
    it('How it works', function () {
        cy.contains('How it works').click({force: true})   
    })     /*
    it('Imagine', function () {       
        cy.contains('Data Times').click({force: true})
            cy.url()
                .should('include', '/')           
    })
    it('About', function () {       
        cy.contains('About').click({force: true})
            cy.url()
                .should('include', '/about')           
    })
    it('Support', function () {       
    cy.contains('Support').click({force: true})
        cy.url()
            .should('include', '/#the-data-times')           
    })
    it('Contact', function () {
        cy.contains('Contact')
    })
    it('0.2', function () {
        cy.contains('0.2')
    })
    it('Input-bar', function () {
        cy.get('[type="text"]').clear()
    })
    it('Search Data Times', function () {
        cy.contains('Search Data Times').click({force: true}) 
    })
    it('Check-box', function () {
        cy.get('[type="checkbox"]').check()
        cy.get('[type="checkbox"]').uncheck()
    }) 
    it('Select-menu', function () {
        cy.get('select').select('Descending') 
    }) 
    it('?', function () {
        cy.get('[role = "img"]')
    })
    it('<<', function () {
        cy.contains('«').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=domestic%20violence')
    })
    it('‹', function () {
        cy.contains('‹').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=domestic%20violence')
    })
    it('1', function () {
        cy.contains('1').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=domestic%20violence')
    })
    it('2', function () {
        cy.contains('2').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=domestic%20violence')
    })
    it('3', function () {
        cy.contains('3').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=domestic%20violence')
    })    
    it('…', function () {
        cy.contains('…').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=domestic%20violence')
    })
    it('›', function () {
        cy.contains('›').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=domestic%20violence')
    })
    it('»', function () {
        cy.contains('»').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=domestic%20violence')
    })   
*/



/*
    describe('Fire statistics: Fatalities and casualties', function () {
        beforeEach(function () {
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=domestic%20violence') 
                    cy.get('select').select('Descending')          
            })
        it('Fire statistics: Fatalities and casualties', function () {
            cy.contains('Fire statistics: Fatalities and casualties').click({force: true})
            cy.url()
                .should('include', '/dataset/b1KvO20B-jQaiMLCG_MW')
        }) 
        it('domestic violence', function () {
            cy.contains('domestic violence').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=domestic%20violence')
        })
        it('operations', function () {
            cy.contains('operations').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=operations')
        })
        it('hospitals', function () {
            cy.contains('hospitals').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=hospitals')
        })  
        it('doctors', function () {
            cy.contains('doctors').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=doctors')
        }) 
        it('health', function () {
            cy.contains('health').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=health')
        })
        it('gps', function () {
            cy.contains('gps').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=gps')
        })
        it('crime', function () {
            cy.contains('crime').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=crime')
        })
        it('waiting times', function () {
            cy.contains('waiting times').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=waiting%20times')
        })  
        it('Add favourite', function () {
            cy.get('[role = "img"]')
        })
        it('Explore Data', function () {
            cy.contains('Explore Data').click()
        })
    }) */








/*
    describe('Aid Worker KKA (Killed, Kidnapped or Arrested)', function () {
        beforeEach(function () {
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=domestic%20violence') 
                    cy.get('select').select('Descending')          
            })
        it('Aid Worker KKA (Killed, Kidnapped or Arrested)', function () {
            cy.contains('Aid Worker KKA (Killed, Kidnapped or Arrested)').click({force: true})
            cy.url()
                .should('include', '/dataset/bludX20BL3OQCrODuiWz')
        }) 
        it('domestic violence', function () {
            cy.contains('domestic violence').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=domestic%20violence')
        })
        it('asylum seeker', function () {
            cy.contains('asylum seeker').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=asylum%20seeker')
        })
        it('waiting times', function () {
            cy.contains('waiting times').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=waiting%20times')
        }) 
        it('refugee', function () {
            cy.contains('refugee').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=refugee')
         })
         it('immigration', function () {
            cy.contains('immigration').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=immigration')
        })
        it('domestic abuse', function () {
            cy.contains('domestic abuse').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=domestic%20abuse')
        })
        it('gps', function () {
            cy.contains('gps').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=gps')
        })
        it('crime', function () {
            cy.contains('crime').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=crime')
        })
        it('health', function () {
            cy.contains('health').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=health')
        })
        it('Add favourite', function () {
            cy.get('[role = "img"]')
        })
        it('Explore Data', function () {
            cy.contains('Explore Data').click()
        })
    })  */







/*
    describe('Serious pollution incidents affecting water, air and land 2002 to 2010', function () {
        beforeEach(function () {
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=domestic%20violence') 
                    cy.get('select').select('Descending')          
            })
        it('Serious pollution incidents affecting water, air and land 2002 to 2010', function () {
            cy.contains('Serious pollution incidents affecting water, air and land 2002 to 2010').click({force: true})
            cy.url()
                .should('include', '/dataset/CTidQW0B7IJTb33tLZhr')
        }) 
        it('domestic violence', function () {
            cy.contains('domestic violence').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=domestic%20violence')
        })
        it('policing', function () {
            cy.contains('policing').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=policing')
        })
        it('environment', function () {
            cy.contains('environment').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=environment')
        })
        it('victims', function () {
            cy.contains('victims').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=victims')
        })
        it('crime', function () {
            cy.contains('crime').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=crime')
        })
        it('domestic abuse', function () {
            cy.contains('domestic abuse').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=domestic%20abuse')
        })
        it('child abuse', function () {
            cy.contains('child abuse').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=child%20abuse')
        }) 
        it('pollution', function () {                                        //no cypress problem                                       
            cy.contains('pollution').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=pollution')
        })
        it('Add favourite', function () {
            cy.get('[role = "img"]')
        })
        it('Explore Data', function () {
            cy.contains('Explore Data').click()
        })
    }) */









/*
    describe('Global Burden of Armed Violence', function () {
        beforeEach(function () {
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=domestic%20violence') 
                    cy.get('select').select('Descending')          
            })
            it('Global Burden of Armed Violence', function () {
                cy.contains('Global Burden of Armed Violence').click({force: true})
                cy.url()
                    .should('include', '/dataset/Qzh1YW0B7IJTb33tFp0j')
            }) 
        it('domestic violence', function () {
            cy.contains('domestic violence').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=domestic%20violence')
        })
        it('policing', function () {
            cy.contains('policing').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=policing')
        })
        it('child abuse', function () {
            cy.contains('child abuse').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=child%20abuse')
        }) 
        it('victims', function () {
            cy.contains('victims').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=victims')
        })
        it('trafficking', function () {
            cy.contains('trafficking').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=trafficking')
        })
        it('crime', function () {
            cy.contains('crime').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=crime')
        })
        it('domestic abuse', function () {
            cy.contains('domestic abuse').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=domestic%20abuse')
        })
        it('Add favourite', function () {
            cy.get('[role = "img"]')
        })
        it('Explore Data', function () {
            cy.contains('Explore Data').click()
        })
    })  */









/*

    describe('The Small Arms Survey Database on Violent Deaths', function () {
        beforeEach(function () {
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=domestic%20violence') 
                    cy.get('select').select('Descending')          
            })
            it('The Small Arms Survey Database on Violent Deaths', function () {
                cy.contains('The Small Arms Survey Database on Violent Deaths').click({force: true})
                cy.url()
                    .should('include', '/dataset/n52WYG0BdeI7J_hLSLbB')
            }) 
            it('domestic violence', function () {
                cy.contains('domestic violence').click({force: true})
                cy.url()
                    .should('include', '/searchresults?cats=domestic%20violence')
            })
            it('victims', function () {
                cy.contains('victims').click({force: true})
                cy.url()
                    .should('include', '/searchresults?cats=victims')
            })
            it('child abuse', function () {
                cy.contains('child abuse').click({force: true})
                cy.url()
                    .should('include', '/searchresults?cats=child%20abuse')
            }) 
            it('policing', function () {
                cy.contains('policing').click({force: true})
                cy.url()
                    .should('include', '/searchresults?cats=policing')
            })
            it('domestic abuse', function () {
                cy.contains('domestic abuse').click({force: true})
                cy.url()
                    .should('include', '/searchresults?cats=domestic%20abuse')
            })
            it('crime', function () {
                cy.contains('crime').click({force: true})
                cy.url()
                    .should('include', '/searchresults?cats=crime')
            })
            it('verdicts', function () {
                cy.contains('verdicts').click({force: true})
                cy.url()
                    .should('include', '/searchresults?cats=verdicts')
            })
            it('Add favourite', function () {
                cy.get('[role = "img"]')
            })
            it('Explore Data', function () {
                cy.contains('Explore Data').click()
            })
        })                          */









        describe('Organogram of Staff Roles & Salaries', function () {
            beforeEach(function () {
                    cy.visit('https://dev.thedatatimes.com/searchresults?cats=domestic%20violence') 
                        cy.get('select').select('Descending')          
                })
                it('Organogram of Staff Roles & Salaries', function () {
                    cy.contains('Organogram of Staff Roles & Salaries').click({force: true})
                    cy.url()
                        .should('include', '/dataset/C50HQW0BdeI7J_hLvq31')
                }) 
                it('domestic violence', function () {
                    cy.contains('domestic violence').click({force: true})
                    cy.url()
                        .should('include', '/searchresults?cats=domestic%20violence')
                })
                it('policing', function () {
                    cy.contains('policing').click({force: true})
                    cy.url()
                        .should('include', '/searchresults?cats=policing')
                })
                it('crime', function () {
                    cy.contains('crime').click({force: true})
                    cy.url()
                        .should('include', '/searchresults?cats=crime')
                })
                it('domestic abuse', function () {
                    cy.contains('domestic abuse').click({force: true})
                    cy.url()
                        .should('include', '/searchresults?cats=domestic%20abuse')
                })
                it('verdicts', function () {
                    cy.contains('verdicts').click({force: true})
                    cy.url()
                        .should('include', '/searchresults?cats=verdicts')
                })
                it('prison officers', function () {
                    cy.contains('prison officers').click({force: true})
                    cy.url()
                        .should('include', '/searchresults?cats=prison%20officers')
                })
                it('court', function () {
                    cy.contains('court').click({force: true})
                    cy.url()
                        .should('include', '/searchresults?cats=court')
                })
                it('Add favourite', function () {
                    cy.get('[role = "img"]')
                })
                it('Explore Data', function () {
                    cy.contains('Explore Data').click()
                })    
                                        
            })



})