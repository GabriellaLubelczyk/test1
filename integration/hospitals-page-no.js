describe('Test hospitals page ', function () {
    beforeEach(function () {
        cy.visit('https://dev.thedatatimes.com/searchresults?cats=hospitals')           
    })
    it('How it works', function () {
        cy.contains('How it works').click({force: true})   
    })     /*
    it('Imagine', function () {       
        cy.contains('Data Times').click({force: true})
            cy.url()
                .should('include', '/')           
    })
    it('About', function () {       
        cy.contains('About').click({force: true})
            cy.url()
                .should('include', '/about')           
    })
    it('Support', function () {       
    cy.contains('Support').click({force: true})
        cy.url()
            .should('include', '/#the-data-times')           
    })
    it('Contact', function () {
        cy.contains('Contact')
    })
    it('0.2', function () {
        cy.contains('0.2')
    })
    it('Input-bar', function () {
        cy.get('[type="text"]').clear()
    })
    it('Search Data Times', function () {
        cy.contains('Search Data Times').click({force: true}) 
    })
    it('Check-box', function () {
        cy.get('[type="checkbox"]').check()
        cy.get('[type="checkbox"]').uncheck()
    }) 
    it('Select-menu', function () {
        cy.get('select').select('Descending') 
    }) 
    it('?', function () {
        cy.get('[role = "img"]')
    })
    it('<<', function () {
        cy.contains('«').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=hospitals')
    })
    it('‹', function () {
        cy.contains('‹').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=hospitals')
    })
    it('1', function () {
        cy.contains('1').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=hospitals')
    })
    it('2', function () {
        cy.contains('2').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=hospitals')
    })
    it('3', function () {
        cy.contains('3').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=hospitals')
    })    
    it('…', function () {
        cy.contains('…').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=hospitals')
    })
    it('›', function () {
        cy.contains('›').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=hospitals')
    })
    it('»', function () {
        cy.contains('»').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=hospitals')
    })   */





/*
    describe('Whole of Afghanistan Assessment - Hard to Reach Dataset - August 2018', function () {
        beforeEach(function () {
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=hospitals') 
                    cy.get('select').select('Descending')          
            })
        it('Whole of Afghanistan Assessment - Hard to Reach Dataset - August 2018', function () {
            cy.contains('Whole of Afghanistan Assessment - Hard to Reach Dataset - August 2018').click({force: true})
            cy.url()
                .should('include', '/dataset/w9IaqmwBz1n8x7rRpoi-')
        }) 
        it('hospitals', function () {
            cy.contains('hospitals').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=hospitals')
        })    
        it('ethnic group', function () {
            cy.contains('ethnic group (to be added)').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=ethnic%20group%20%28to%20be%20added%29')
        })
        it('lgbtq', function () {
            cy.contains('lgbtq').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=lgbtq')
        })
        it('gps', function () {
            cy.contains('gps').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=gps')
        })
        it('environment', function () {
            cy.contains('environment').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=environment')
        })
        it('health', function () {
            cy.contains('health').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=health')
        })
        it('equality', function () {
            cy.contains('equality').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=equality')
        })
        it('education', function () {
            cy.contains('education').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=education')
        })
        it('operations', function () {
            cy.contains('operations').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=operations')
        })
        it('transport', function () {
            cy.contains('transport').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=transport')
        })
        it('technology', function () {
            cy.contains('technology').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=technology')
        })
        it('climate change', function () {
            cy.contains('climate change').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=climate%20change')
        })
        it('recycling', function () {
            cy.contains('recycling').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=recycling')
        })
        it('plastics', function () {
            cy.contains('plastics').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=plastics')
        })
        it('disabilities', function () {
            cy.contains('disabilities').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=disabilities')
        })
        it('driver', function () {
            cy.contains('driver').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=driver')
        })
        it('a levels', function () {
            cy.contains('a levels').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=a%20levels')
        })
        it('global warming', function () {
            cy.contains('global warming').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=global%20warming')
        }) 
        it('race', function () {
            cy.contains('race').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=race')
        }) 
        it('open source', function () {
            cy.contains('open source').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=open%20source')
        }) 
        it('pollution', function () {
            cy.contains('pollution').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=pollution')
        })
        it('inclusion', function () {
            cy.contains('inclusion').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=inclusion')
        }) 
        it('cycle lanes', function () {
            cy.contains('cycle lanes').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=cycle%20lanes')
        })
        it('waiting times', function () {
            cy.contains('waiting times').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=waiting%20times')
        })
        it('vehicle', function () {
            cy.contains('vehicle').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=vehicle')
        })
        it('Add favourite', function () {
            cy.get('[role = "img"]')
        })
        it('Explore Data', function () {
            cy.contains('Explore Data').click()
        }) 
    })*/






/*
describe('Health and Wellbeing Profiles for Community Health Partnership (CHP) Areas', function () {
        beforeEach(function () {
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=hospitals') 
                    cy.get('select').select('Descending')          
            })
        it('Health and Wellbeing Profiles for Community Health Partnership (CHP) Areas', function () {
            cy.contains('Health and Wellbeing Profiles for Community Health Partnership (CHP) Areas').click({force: true})
            cy.url()
                .should('include', '/dataset/BJ0HQW0BdeI7J_hLlK0q')
        }) 
        it('hospitals', function () {
            cy.contains('hospitals').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=hospitals')
        })    
        it('operations', function () {
            cy.contains('operations').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=operations')
        })
        it('waiting times', function () {
            cy.contains('waiting times').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=waiting%20times')
        })
        it('health', function () {
            cy.contains('health').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=health')
        })
        it('dentists', function () {
            cy.contains('dentists').click({force: true})
            cy.url()
        })
        it('gps', function () {
            cy.contains('gps').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=gps')
        })
        it('nhs', function () {
            cy.contains('nhs').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=nhs')
        })
        it('Add favourite', function () {
            cy.get('[role = "img"]')
        })
        it('Explore Data', function () {
            cy.contains('Explore Data').click()
        }) 
    }) */








/*

    describe('Northern Ireland Emergency Care Waiting Time Statistics', function () {
        beforeEach(function () {
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=hospitals') 
                    cy.get('select').select('Descending')          
            })
        it('Northern Ireland Emergency Care Waiting Time Statistics', function () {
            cy.contains('Northern Ireland Emergency Care Waiting Time Statistics').click({force: true})
            cy.url()
                .should('include', '/dataset/WVKuO20B-jQaiMLCSvMy')
        }) 
        it('hospitals', function () {
            cy.contains('hospitals').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=hospitals')
        })    
        it('operations', function () {
            cy.contains('operations').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=operations')
        })
        it('health', function () {
            cy.contains('health').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=health')
        })
        it('gps', function () {
            cy.contains('gps').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=gps')
        })
        it('waiting times', function () {
            cy.contains('waiting times').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=waiting%20times')
        })
        it('waiting lists', function () {
            cy.contains('waiting lists').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=waiting%20lists')
        })
        it('Add favourite', function () {
            cy.get('[role = "img"]')
        })
        it('Explore Data', function () {
            cy.contains('Explore Data').click()
        }) 
    }) */












/*

    describe('GP Access Survey', function () {
        beforeEach(function () {
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=hospitals') 
                    cy.get('select').select('Descending')          
            })
        it('GP Access Survey', function () {
            cy.contains('GP Access Survey').click({force: true})
            cy.url()
                .should('include', '/dataset/XFKuO20B-jQaiMLCg_Pa')
        }) 
        it('hospitals', function () {
            cy.contains('hospitals').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=hospitals')
        })     
        it('operations', function () {
            cy.contains('operations').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=operations')
        })
        it('nhs', function () {
            cy.contains('nhs').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=nhs')
        })
        it('dentists', function () {
            cy.contains('dentists').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=dentists')
        })
        it('health', function () {
            cy.contains('health').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=health')
        })
        it('gps', function () {
            cy.contains('gps').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=gps')
        })
        it('waiting times', function () {
            cy.contains('waiting times').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=waiting%20times')
        })
        it('Add favourite', function () {
            cy.get('[role = "img"]')
        })
        it('Explore Data', function () {
            cy.contains('Explore Data').click()
        })
    })          */









/*
    describe('Public Health Intelligence 2011 Census Ward Profiles', function () {
        beforeEach(function () {
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=hospitals') 
                    cy.get('select').select('Descending')          
            })
        it('Public Health Intelligence 2011 Census Ward Profiles', function () {
            cy.contains('Public Health Intelligence 2011 Census Ward Profiles').click({force: true})
            cy.url()
                .should('include', '/dataset/blKvO20B-jQaiMLCEvOM')
        }) 
        it('hospitals', function () {
            cy.contains('hospitals').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=hospitals')
        })     
        it('operations', function () {
            cy.contains('operations').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=operations')
        })
        it('nhs', function () {
            cy.contains('nhs').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=nhs')
        })
        it('health', function () {
            cy.contains('health').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=health')
        })
        it('gps', function () {
            cy.contains('gps').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=gps')
        })
        it('waiting times', function () {
            cy.contains('waiting times').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=waiting%20times')
        })
        it('waiting lists', function () {
            cy.contains('waiting lists').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=waiting%20lists')
        })
        it('Add favourite', function () {
            cy.get('[role = "img"]')
        })
        it('Explore Data', function () {
            cy.contains('Explore Data').click()
        })
    })   */






/*
    describe('Fire statistics: Fatalities and casualties', function () {
        beforeEach(function () {
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=hospitals') 
                    cy.get('select').select('Descending')          
            })
        it('Fire statistics: Fatalities and casualties', function () {
            cy.contains('Fire statistics: Fatalities and casualties').click({force: true})
            cy.url()
                .should('include', '/dataset/b1KvO20B-jQaiMLCG_MW')
        }) 
        it('hospitals', function () {
            cy.contains('hospitals').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=hospitals')
        })  
        it('operations', function () {
            cy.contains('operations').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=operations')
        })
        it('doctors', function () {
            cy.contains('doctors').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=doctors')
        })
        it('health', function () {
            cy.contains('healt').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=health')
        })
        it('domestic violence', function () {
            cy.contains('domestic violence').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=domestic%20violence')
        })
        it('gps', function () {
            cy.contains('gps').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=gps')
        })
        it('crime', function () {
            cy.contains('crime').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=crime')
        })
        it('waiting times', function () {
            cy.contains('waiting times').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=waiting%20times')
        })
        it('Add favourite', function () {
            cy.get('[role = "img"]')
        })
        it('Explore Data', function () {
            cy.contains('Explore Data').click()
        })
    })  */







    describe('Denmark - Health Indicators', function () {
        beforeEach(function () {
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=hospitals') 
                    cy.get('select').select('Descending')          
            })
        it('Denmark - Health Indicators', function () {
            cy.contains('Denmark - Health Indicators').click({force: true})
            cy.url()
                .should('include', '/dataset/PFviQG0BL3OQCrODuR0S')
        }) 
        it('hospitals', function () {
            cy.contains('hospitals').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=hospitals')
        }) 
        it('operations', function () {
            cy.contains('operations').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=operations')
        })
        it('waiting times', function () {
            cy.contains('waiting times').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=waiting%20times')
        })
        it('waiting lists', function () {
            cy.contains('waiting lists').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=waiting%20lists')
        })
        it('health', function () {
            cy.contains('health').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=health')
        })
        it('dentists', function () {
            cy.contains('dentists').click({force: true})
            cy.url()
        })
        it('gps', function () {
            cy.contains('gps').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=gps')
        })
        it('Add favourite', function () {
            cy.get('[role = "img"]')
        })
        it('Explore Data', function () {
            cy.contains('Explore Data').click()
        })
    })
})