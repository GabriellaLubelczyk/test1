describe('Test environment page ', function () {
    beforeEach(function () {
        cy.visit('https://dev.thedatatimes.com/searchresults?cats=environment')           
    })
    it('How it works', function () {
        cy.contains('How it works').click({force: true})   
    })     /*
    it('Imagine', function () {       
        cy.contains('Data Times').click({force: true})
            cy.url()
                .should('include', '/')           
    })
    it('About', function () {       
        cy.contains('About').click({force: true})
            cy.url()
                .should('include', '/about')           
    })
    it('Support', function () {       
    cy.contains('Support').click({force: true})
        cy.url()
            .should('include', '/#the-data-times')           
    })
    it('Contact', function () {
        cy.contains('Contact')
    })
    it('0.2', function () {
        cy.contains('0.2')
    })
    it('Input-bar', function () {
        cy.get('[type="text"]').clear()
    })
    it('Search Data Times', function () {
        cy.contains('Search Data Times').click({force: true}) 
    })
    it('Check-box', function () {
        cy.get('[type="checkbox"]').check()
        cy.get('[type="checkbox"]').uncheck()
    }) 
    it('Select-menu', function () {
        cy.get('select').select('Descending') 
    }) 
    it('?', function () {
        cy.get('[role = "img"]')
    })
    it('<<', function () {
        cy.contains('«').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=environment')
    })
    it('‹', function () {
        cy.contains('‹').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=environment')
    })
    it('1', function () {
        cy.contains('1').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=environment')
    })
    it('2', function () {
        cy.contains('2').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=environment')
    })
    it('3', function () {
        cy.contains('3').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=environment')
    })    
    it('…', function () {
        cy.contains('…').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=environment')
    })
    it('›', function () {
        cy.contains('›').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=environment')
    })
    it('»', function () {
        cy.contains('»').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=environment')
    })  
*/


/*
    describe('Uruguay administrative', function () {
        beforeEach(function () {
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=environment') 
                    cy.get('select').select('Descending')          
            })
        it('Uruguay administrative ', function () {
            cy.contains('Uruguay administrative level 0, 1, and 2 boundaries.').click({force: true})
            cy.url()
                .should('include', '/dataset/aJ3bX20BdeI7J_hLb7QR')
        }) 
        it('environment', function () {
            cy.contains('environment').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=environment')
        })
        it('water', function () {
            cy.contains('water').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=water')
        })
        it('(need to add all gov departments).', function () {
            cy.contains('(need to add all gov departments).').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=%28need%20to%20add%20all%20gov%20departments%29.')
        })
        it('plastics', function () {
            cy.contains('plastics').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=plastics')
        })
        it('lgbtq', function () {
            cy.contains('lgbtq').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=lgbtq')
        })
        it('education', function () {
            cy.contains('education').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=education')
        })
        it('sport', function () {
            cy.contains('sport').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=sport')
        })
        it('inclusion', function () {
            cy.contains('inclusion').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=inclusion')
        })
        it('a levels', function () {
            cy.contains('a levels').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=a%20levels')
        })
        it('politics', function () {
            cy.contains('politics').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=politics')
        })
        it('watersports', function () {
            cy.contains('watersports').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=watersports')
        })
        it('Add favourite', function () {
            cy.get('[role = "img"]')
        })
        it('Explore Data', function () {
            cy.contains('Explore Data').click()
        })
    })     */











/*
    describe('Belfast Events', function () {
        beforeEach(function () {
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=environment') 
                    cy.get('select').select('Descending')          
            })
        it('Belfast Events', function () {
            cy.contains('Belfast Events').click({force: true})
            cy.url()
                .should('include', '/dataset/RVTCDW0Bmah2eTSCjXp2')
        }) 
        it('environment', function () {
            cy.contains('environment').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=environment')
        })
        it('biking', function () {
            cy.contains('biking').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=biking')
        }) 
        it('race', function () {
            cy.contains('race').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=race')
        }) 
        it('recycling', function () {
            cy.contains('recycling').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=recycling')
            })
            it('watersports', function () {
                cy.contains('watersports').click({force: true})
                cy.url()
                    .should('include', '/searchresults?cats=watersports')
            })
            it('inclusion', function () {
                cy.contains('inclusion').click({force: true})
                cy.url()
                    .should('include', '/searchresults?cats=inclusion')
            })
            it('hall', function () {
                cy.contains('hall').click({force: true})
                cy.url()
                    .should('include', '/searchresults?cats=hall')
            })
            it('lgbtq', function () {
                cy.contains('lgbtq').click({force: true})
                cy.url()
                    .should('include', '/searchresults?cats=lgbtq')
            })
            it('sport', function () {
                cy.contains('sport').click({force: true})
                cy.url()
                    .should('include', '/searchresults?cats=sport')
            })
            it('Add favourite', function () {
                cy.get('[role = "img"]')
            })
            it('Explore Data', function () {
                cy.contains('Explore Data').click()
            })
    }) */







/*
    describe('Anguilla administrative ', function () {
        beforeEach(function () {
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=environment') 
                    cy.get('select').select('Descending')          
            })
        it('Anguilla administrative ', function () {
            cy.contains('Anguilla administrative level 0 and 1 population statistics').click({force: true})
            cy.url()
                .should('include', '/dataset/pp2WYG0BdeI7J_hLh7ZJ')
        }) 
        it('environment', function () {
            cy.contains('environment').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=environment')
        })
        it('global warming', function () {
            cy.contains('global warming').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=global%20warming')
        })
        it('climate change', function () {
            cy.contains('climate change').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=climate%20change')
        })
        it('inclusion', function () {
            cy.contains('inclusion').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=inclusion')
        })
        it('water', function () {
            cy.contains('water').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=water')
        })
        it('disabilities', function () {
            cy.contains('disabilities').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=disabilities')
        })
        it('(need to add all gov departments).', function () {
            cy.contains('(need to add all gov departments).').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=%28need%20to%20add%20all%20gov%20departments%29.')
        })
        it('sport', function () {
            cy.contains('sport').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=sport')
        })
        it('politics', function () {
            cy.contains('politics').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=politics')
        })
        it('lgbtq', function () {
            cy.contains('lgbtq').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=lgbtq')
        })
        it('Add favourite', function () {
            cy.get('[role = "img"]')
        })
        it('Explore Data', function () {
            cy.contains('Explore Data').click()
        })
    })   */






 describe('Chile administrative', function () {
        beforeEach(function () {
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=environment') 
                    cy.get('select').select('Descending')          
            })
        it('Chile administrative level 0, 1, 2, and 3 boundaries', function () {
            cy.visit('https://dev.thedatatimes.com/searchresults?cats=environment')
            cy.contains('Chile administrative level 0, 1, 2, and 3 boundaries').click({force: true})
            cy.url()
                .should('include', '/dataset/uJ2WYG0BdeI7J_hLu7aO')
        }) 
        it('environment', function () {
            cy.contains('environment').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=environment')
        })
    })   //NO EXIST






/*
    describe('Local area walking and cycling statistics', function () {
        beforeEach(function () {
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=environment') 
                    cy.get('select').select('Descending')          
            })
        it('Local area walking and cycling statistics', function () {
            cy.contains('Local area walking and cycling statistics').click({force: true})
            cy.url()
                .should('include', '/dataset/llsNQW0BL3OQCrOD_iDQ')
        }) 
        it('environment', function () {
            cy.contains('environment').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=environment')
        })
        it('fitness', function () {
            cy.contains('fitness').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=fitness')
        })
        it('biking', function () {
            cy.contains('biking').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=biking')
        }) 
        it('water', function () {
            cy.contains('water').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=water')
        })
        it('mountain', function () {
            cy.contains('mountain').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=mountain')
        })
        it('sport', function () {                           //don't work
            cy.contains('sport').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=sport')
        })
        it('swimming', function () {
            cy.contains('swimming').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=swimming')
        })
        it('recycling', function () {
            cy.contains('recycling').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=recycling')
        })
        it('Add favourite', function () {
            cy.get('[role = "img"]')
        })
        it('Explore Data', function () {
            cy.contains('Explore Data').click()
        })
    }) */







/*
    describe('World - Water Body Limits Centroids', function () {
        beforeEach(function () {
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=environment') 
                    cy.get('select').select('Descending')          
            })
        it('World - Water Body Limits Centroids', function () {
            cy.contains('World - Water Body Limits Centroids').click({force: true})
            cy.url()
                .should('include', '/dataset/6p2mX20BdeI7J_hLs7LZ')
        }) 
        it('environment', function () {
            cy.contains('environment').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=environment')
        })
        it('water', function () {
            cy.contains('water').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=water')
        })
        it('climate change', function () {
            cy.contains('climate change').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=climate%20change')
        })
        it('pollution', function () {
            cy.contains('pollution').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=pollution')
        })
        it('plastics', function () {
            cy.contains('plastics').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=plastics')
        })
        it('sport', function () {
            cy.contains('sport').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=sport')
        })
        it('global warming', function () {
            cy.contains('global warming').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=global%20warming')
        })
        it('recycling', function () {
        cy.contains('recycling').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=recycling')
        })
        it('Add favourite', function () {
            cy.get('[role = "img"]')
        })
        it('Explore Data', function () {
            cy.contains('Explore Data').click()
        })
    }) */







/*
    describe('Brazil administrative level 0, 1, 2, and 3 boundaries', function () {
        beforeEach(function () {
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=environment') 
                    cy.get('select').select('Descending')          
            })
        it('Brazil administrative level 0, 1, 2, and 3 boundaries', function () {
            cy.contains('Brazil administrative level 0, 1, 2, and 3 boundaries').click({force: true})
            cy.url()
                .should('include', '/dataset/Hp22QW0BdeI7J_hL-7FV')
        }) 
        it('environment', function () {
            cy.contains('environment').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=environment')
        })
        it('climate change', function () {
            cy.contains('climate change').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=climate%20change')
        })
        it('plastics', function () {
            cy.contains('plastics').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=plastics')
        })
        it('sport', function () {
            cy.contains('sport').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=sport')
        })
        it('pollution', function () {
            cy.contains('pollution').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=pollution')
        })
        it('sand', function () {
            cy.contains('sand').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=sand')
        })
        it('recycling', function () {
            cy.contains('recycling').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=recycling')
        })
        it('water', function () {
            cy.contains('water').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=water')
        })
        it('Add favourite', function () {
            cy.get('[role = "img"]')
        })
        it('Explore Data', function () {
            cy.contains('Explore Data').click()
        })
    }) */







/*
    describe('Saint Kitts and Nevis administrative level 0 (nation) and 1 (parish) boundaries.', function () {
        beforeEach(function () {
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=environment') 
                    cy.get('select').select('Descending')          
            })
        it('Saint Kitts and Nevis administrative level 0 (nation) and 1 (parish) boundaries.', function () {
            cy.contains('Saint Kitts and Nevis administrative level 0 (nation) and 1 (parish) boundaries.').click({force: true})
            cy.url()
                .should('include', '/dataset/aTi3QW0B7IJTb33tO5iO')
        }) 
        it('environment', function () {
            cy.contains('environment').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=environment')
        })
        it('climate change', function () {
            cy.contains('climate change').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=climate%20change')
        })
        it('education', function () {
            cy.contains('education').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=education')
        })
        it('sport', function () {
            cy.contains('sport').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=sport')
        })
        it('plastics', function () {
            cy.contains('plastics').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=plastics')
        })
        it('a levels', function () {
            cy.contains('a levels').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=a%20levels')
        })
        it('recycling', function () {
            cy.contains('recycling').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=recycling')
        })
        it('global warming', function () {
            cy.contains('global warming').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=global%20warming')
        })
        it('water', function () {
            cy.contains('water').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=water')
        })
        it('Add favourite', function () {
            cy.get('[role = "img"]')
        })
        it('Explore Data', function () {
            cy.contains('Explore Data').click()
        })
    }) */
})   