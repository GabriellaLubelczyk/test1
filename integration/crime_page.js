 describe('Test Crime Page ', function () {
    beforeEach(function () {
        cy.visit('https://dev.thedatatimes.com/searchresults?cats=Crime')           
    })
    it('How it works', function () {
        cy.contains('How it works').click()   
    })
    it('Imagine', function () {       
        cy.contains('Data Times').click({force: true})
            cy.url()
                .should('include', '/')           
    })
    it('About', function () {       
        cy.contains('About').click({force: true})
            cy.url()
                .should('include', '/about')           
    })
    it('Support', function () {       
    cy.contains('Support').click({force: true})
        cy.url()
            .should('include', '/#the-data-times')           
    })
    it('Contact', function () {
        cy.contains('Contact')
    })
    it('0.2', function () {
        cy.contains('0.2')
    })
    it('Input-bar', function () {
        cy.get('[type="text"]').clear()
    })
    it('Search Data Times', function () {
        cy.contains('Search Data Times').click({force: true}) 
    })
    it('Check-box', function () {
        cy.get('[type="checkbox"]').check()
        cy.get('[type="checkbox"]').uncheck()
    })
    it('Check-box single element prove 1', function () {
        cy.get('HDX').within(() => { 
            cy.get('[type="checkbox"]').check()
        })
    })
    it('Check-box single element prove 2', function () {
        cy.get('[type="radio"]').first().check()
    })
    it('Select-menu', function () {
        cy.get('select').select('Descending') 
    })
    it('?', function () {
        cy.get('[role = "img"]')
    })
    it('<<', function () {
        cy.contains('«').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=Crime')
    })
    it('‹', function () {
        cy.contains('‹').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=Crime')
    })
    it('1', function () {
        cy.contains('1').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=Crime')
    })
    it('2', function () {
        cy.contains('2').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=Crime')
    })
    it('3', function () {
        cy.contains('3').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=Crime')
    })    
    it('…', function () {
        cy.contains('…').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=Crime')
    })
    it('›', function () {
        cy.contains('›').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=Crime')
    })
    it('»', function () {
        cy.contains('»').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=Crime')
    }) 
    





describe('Fire ', function () {
    beforeEach(function () {
            cy.visit('https://dev.thedatatimes.com/searchresults?cats=Crime')           
        })
    it('Fire statistics', function () {
        cy.contains('Fire statistics: Fatalities and casualties').click({force: true})
        cy.url()
            .should('include', '/dataset/b1KvO20B-jQaiMLCG_MW')
    })
    it('operations', function () {
        cy.contains('operations').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=operations')
    })
    it('hospitals', function () {
        cy.contains('hospitals').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=hospitals')
    })
     it('doctors', function () {
        cy.contains('doctors').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=doctors')
    })
    it('health', function () {
        cy.contains('health').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=health')
    })
    it('domestic violence', function () {
        cy.contains('domestic violence').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=domestic%20violence')
    })
    it('gps', function () {
        cy.contains('gps').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=gps')
    })
    it('crime', function () {
        cy.contains('crime').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=crime')
    })
    it('waiting times', function () {
        cy.contains('waiting times').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=waiting%20times')
    }) 
    it('Add favourite', function () {
        cy.get('[role = "img"]')
    })
    it('Explore Data', function () {
        cy.contains('Explore Data').click()
    }) 
}) 








/*

    describe('Guatemala ', function () { 
        beforeEach(function () {
            cy.visit('https://dev.thedatatimes.com/searchresults?cats=Crime')           
        })
        it('Guatemala', function () {
            cy.contains('Guatemala - Who is Doing What Where (3W)').click({force: true})
            cy.url()
                .should('include', '/dataset/R52PX20BdeI7J_hLmLJr')
        })
        it('waiting times', function () {
            cy.contains('waiting times').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=waiting%20times')
        })
        it('doctors', function () {
            cy.contains('doctors').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=doctors')
        })
        it('brexit', function () {
            cy.contains('brexit').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=brexit')
        })
        it('trafficking', function () {
            cy.contains('trafficking').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=trafficking')
        })
        it('ombudsman', function () {
            cy.contains('ombudsman').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=ombudsman')
        })
        it('politics', function () {
            cy.contains('politics').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=politics')
        })
        it('health', function () {
            cy.contains('health').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=health')
        })
        it('Add favourite', function () {
            cy.get('[role = "img"]')
        })
        it('Explore Data', function () {
            cy.contains('Explore Data').click()
        }) 
    })







    describe('Honduras ', function () { 
        before(function () {
            cy.visit('https://dev.thedatatimes.com/searchresults?cats=Crime')         
        })
        it('Honduras', function () {
            cy.contains('Honduras Población Expuesta a Ciclones 2012').click({force: true})
            cy.url()
                .should('include', '/dataset/RFtzYW0BL3OQCrODnimF')
        })
        it('operations', function () {
            cy.contains('operations').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=operations')
        })
        it('gps', function () {
            cy.contains('gps').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=gps')
        })
        it('ombudsman', function () {
            cy.contains('ombudsman').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=ombudsman')
        })
        it('waiting lists', function () {
            cy.contains('waiting lists').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=waiting%20lists')
        })
        it('appeals', function () {
            cy.contains('appeals').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=appeals')
        })
        it('waiting times', function () {
            cy.contains('waiting times').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=waiting%20times')
        })
        it('health', function () {
            cy.contains('health').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=health')
        })
        it('Add favourite', function () {
            cy.get('[role = "img"]')
        })
        it('Explore Data', function () {
            cy.contains('Explore Data').click()
        }) 
    }) 
  
    
    describe('Drug misuse', function () { 
        beforeEach(function () {
            cy.visit('https://dev.thedatatimes.com/searchresults?cats=Crime')      
        })
        it('Drug misuse', function () {
            cy.contains('Drug misuse').click({force: true})
            cy.url()
                .should('include', '/dataset/yJ1cQW0BdeI7J_hLOa1v')
        })
        it('waiting times', function () {
            cy.contains('waiting times').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=waiting%20times')
        })
        it('gps', function () {
            cy.contains('gps').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=gps')
        })
         it('operations', function () {
            cy.contains('operations').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=operations')
        })
        it('child abuse', function () {
            cy.contains('child abuse').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=child%20abuse')
        })
        it('health', function () {
            cy.contains('health').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=health')
        })
        it('court', function () {
            cy.contains('court').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=court')
        })
        it('medicines', function () {
            cy.contains('medicines').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=medicines')
        })
        it('Add favourite', function () {
            cy.get('[role = "img"]')
        })
        it('Explore Data', function () {
            cy.contains('Explore Data').click()
        }) 
    })

    



    describe('Partial Assessment', function () { 
        beforeEach(function () {
            cy.visit('https://dev.thedatatimes.com/searchresults?cats=Crime')   
        })
        it('Partial Assessment and 4W matrix ,Hurricane Matthew', function () {
            cy.contains('Partial Assessment and 4W matrix ,Hurricane Matthew').click({force: true})
            cy.url()
                .should('include', '/dataset/ITiRYW0B7IJTb33tSJ9w')
        })       
        it('waiting times', function () {
            cy.contains('waiting times').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=waiting%20times')
        })
        it('gps', function () {
            cy.contains('gps').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=gps')
        })
         it('operations', function () {
            cy.contains('operations').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=operations')
        })
        it('child abuse', function () {
            cy.contains('child abuse').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=child%20abuse')
        })
        it('health', function () {
            cy.contains('health').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=health')
        })
        it('medicines', function () {
            cy.contains('medicines').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=medicines')
        })
        it('Add favourite', function () {
            cy.get('[role = "img"]')
        })
        it('Explore Data', function () {
            cy.contains('Explore Data').click()
        }) 
    }) 









    describe('Guatemala Acceso a electricidad 2015', function () { 
        before(function () {
            cy.visit('https://dev.thedatatimes.com')           
        })
        it('Open page', function () {
            cy.visit('https://dev.thedatatimes.com/searchresults?cats=Crime')           
        })
        it('Guatemala Acceso a electricidad 2015', function () {
            cy.contains('Guatemala Acceso a electricidad 2015').click({force: true})
            cy.url()
                .should('include', '/dataset/wZ3UX20BdeI7J_hL0rOI')
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=Crime')
        })
        it('waiting times', function () {
            cy.contains('waiting times').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=waiting%20times')
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=Crime')
        })
        it('environment', function () {
            cy.contains('environment').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=environment')
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=Crime')
        })
         it('pollution', function () {
            cy.contains('pollution').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=pollution')
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=Crime')
        })
        it('waiting lists', function () {
            cy.contains('waiting lists').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=waiting%20lists')
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=Crime')
        })
        it('gps', function () {
            cy.contains('gps').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=gps')
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=Crime')
        })
        it('crime', function () {
            cy.contains('crime').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=crime')
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=Crime')
        })
        it('ombudsman', function () {
            cy.contains('ombudsman').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=ombudsman')
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=Crime')
        })
        it('operations', function () {
            cy.contains('operations').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=operations')
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=Crime')
        })
        it('health', function () {
            cy.contains('health').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=health')
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=Crime')
        })
        it('Add favourite', function () {
            cy.get('[role = "img"]')
        })
        it('Explore Data', function () {
            cy.contains('Explore Data').click()
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=Crime')
        }) 
    }) 







    describe('Aid Worker KKA (Killed, Kidnapped or Arrested)', function () { 
        beforeEach(function () {
            cy.visit('https://dev.thedatatimes.com/searchresults?cats=Crime')       
        })
        it('Aid Worker KKA (Killed, Kidnapped or Arrested)', function () {
            cy.contains('Aid Worker KKA (Killed, Kidnapped or Arrested').click({force: true})
            cy.url()
                .should('include', '/dataset/bludX20BL3OQCrODuiWz')
               
        })
        it('asylum seeker', function () {
            cy.contains('asylum seeker').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=asylum%20seeker')
        })
        it('waiting times', function () {
            cy.contains('waiting times').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=waiting%20times')
        })
         it('refugee', function () {
            cy.contains('refugee').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=refugee')
         })
        it('immigration', function () {
            cy.contains('immigration').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=immigration')
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=Crime')
        })
        it('domestic abuse', function () {
            cy.contains('domestic abuse').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=domestic%20abuse')
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=Crime')
        })
        it('gps', function () {
            cy.contains('gps').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=gps')
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=Crime')
        })
        it('domestic violence', function () {
            cy.contains('domestic violence').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=domestic%20violence')
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=Crime')
        })
        it('crime', function () {
            cy.contains('crime').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=crime')
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=Crime')
        })
        it('health', function () {
            cy.contains('health').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=health')
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=Crime')
        })
        it('Add favourite', function () {
            cy.get('[role = "img"]')
        })
        it('Explore Data', function () {
            cy.contains('Explore Data').click()
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=Crime')
        }





    describe('Drug Related Deaths and Deaths due to Drugs Misuse Registered in Northern Ireland', function () { 
        beforeEach(function () {
            cy.visit('https://dev.thedatatimes.com/searchresults?cats=Crime')          
        })
        it('Drug Related Deaths', function () {
            cy.contains('Drug Related Deaths and Deaths due to Drugs Misuse Registered in Northern Ireland').click({force: true})
            cy.url()
                .should('include', '/dataset/F53_QG0BdeI7J_hLLKz4')
        })
        it('waiting times', function () {
            cy.contains('waiting times').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=waiting%20times')
        })
        it('health', function () {
            cy.contains('health').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=health')
        })
        it('medicines', function () {
            cy.contains('medicines').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=medicines')
        })
        it('waiting lists', function () {
            cy.contains('waiting lists').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=waiting%20lists')
        })
        it('child abuse', function () {
            cy.contains('child abuse').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=child%20abuse')
        })
        it('crime', function () {
            cy.contains('crime').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=crime')
        })
        it('doctors', function () {
            cy.contains('doctors').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=doctors')
        })
        it('operations', function () {
            cy.contains('operations').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=operations')
        })
        it('Add favourite', function () {
            cy.get('[role = "img"]')
        })
        it('Explore Data', function () {
            cy.contains('Explore Data').click()
        }) 
    }) 
 */
}) 
