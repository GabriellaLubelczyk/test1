describe('Test local area walking and cycling statistics  article ', function () {
    beforeEach(function () {
        cy.visit('https://dev.thedatatimes.com/dataset/llsNQW0BL3OQCrOD_iDQ')           
    })
    it('How it works', function () {
        cy.contains('How it works').click({force: true})   
    })   
    it('Imagine', function () {       
        cy.contains('Data Times').click({force: true})
            cy.url()
                .should('include', '/')           
    })
    it('About', function () {       
        cy.contains('About').click({force: true})
            cy.url()
                .should('include', '/about')           
    })
    it('Support', function () {       
    cy.contains('Support').click({force: true})
        cy.url()
            .should('include', '/#the-data-times')           
    })
    it('Contact', function () {
        cy.contains('Contact')
    })
    it('0.2', function () {
        cy.contains('0.2')
    })
    it('Input-bar', function () {
        cy.get('[type="text"]').clear()
    })
    it('Search Data Times', function () {
        cy.contains('Search Data Times').click({force: true}) 
    }) 
    it('?', function () {
        cy.get('[role = "img"]')
    })
    it('Back to Search Results', function () {
        cy.contains('Back to Search Results').click({force: true}) 
        cy.url()
    })
    
    describe('Topics', function () {
        it('fitness', function () {
            cy.contains('fitness').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=fitness')
        })
        it('biking', function () {
            cy.contains('biking').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=biking')
        })
        it('water', function () {
            cy.contains('water').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=water')
        })
        it('mountain', function () {
            cy.contains('mountain').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=mountain')
        })
        it('sport', function () {
            cy.contains('sport').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=sport')
        })
        it('environment', function () {
            cy.contains('environment').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=environment')
        })
        it('swimming', function () {
            cy.contains('swimming').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=swimming')
        })
        it('recycling', function () {
            cy.contains('recycling').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=recycling')
        })
    })
    it('email', function () {
        cy.contains('colin.bainbridge@northyorks.gov.uk') 
    }) 
    it('lintol', function () {
        cy.contains('Lintol')
    })

})










