
describe('Test time-series Central Africa Republic article ', function () {
    beforeEach(function () {
        cy.visit('https://dev.thedatatimes.com/dataset/IjjXX20B7IJTb33t7ps-')           
    })
    it('How it works', function () {
        cy.contains('How it works').click({force: true})   
    })   
    it('Imagine', function () {       
        cy.contains('Data Times').click({force: true})
            cy.url()
                .should('include', '/')           
    })
    it('About', function () {       
        cy.contains('About').click({force: true})
            cy.url()
                .should('include', '/about')           
    })
    it('Support', function () {       
    cy.contains('Support').click({force: true})
        cy.url()
            .should('include', '/#the-data-times')           
    })
    it('Contact', function () {
        cy.contains('Contact')
    })
    it('0.2', function () {
        cy.contains('0.2')
    })
    it('Input-bar', function () {
        cy.get('[type="text"]').clear()
    })
    it('Search Data Times', function () {
        cy.contains('Search Data Times').click({force: true}) 
    }) 
    it('?', function () {
        cy.get('[role = "img"]')
    })
    it('Back to Search Results', function () {
        cy.contains('Back to Search Results').click({force: true}) 
        cy.url()
    })
    
    describe('Topics', function () {
        it('asylum seeker', function () {
            cy.contains('asylum seeker').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=asylum%20seeker')
        })
        it('waiting times', function () {
            cy.contains('waiting times').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=waiting%20times')
        })
        it('health', function () {
            cy.contains('health').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=health')
        })
        it('refugee', function () {
            cy.contains('refugee').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=refugee')
        })
        it('immigration', function () {
            cy.contains('immigration').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=immigration')
        })
        it('housing', function () {
            cy.contains('housing').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=housing')
        })
        it('shelters', function () {
            cy.contains('shelters').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=shelters')
        })
        it('operations', function () {
            cy.contains('operations').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=operations')
        })
        it('hostels', function () {
            cy.contains('hostels').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=hostels')
        })
    })
    it('email', function () {
        cy.contains('pitoiset@unhcr.org')
    })
    it('lintol', function () {
        cy.contains('Lintol')
    })

})





