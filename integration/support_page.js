describe('Test Support Page ', function () {
    it('Open page', function () {
        cy.visit('https://kb.thedatatimes.com')           
    })
    it('Search', function () {
        cy.get('[type="text"]').clear()
    })
    it('The Data Times', function () {
        cy.contains('The Data Times').click({force: true})
        cy.url()
                .should('include', '/#the-data-times')
    })

    it('Overview', function () {
        cy.contains('Overview').click({force: true})
        cy.url()
                .should('include', '/#overview')
    })
    it('Documentation', function () {
        cy.contains('Documentation').click({force: true})
        cy.url()
                .should('include', '/#documentation')
    })
    it('Guides', function () {
        cy.contains('Guides').click({force: true})
        cy.url()
                .should('include', '/#guides')
    })
    it('Searching', function () {
        cy.contains('Searching').click({force: true})
        cy.url()
                .should('include', '/#searching')
    })
    it('Filtering', function () {
        cy.contains('Filtering').click({force: true})
        cy.url()
                .should('include', '/#filtering')
    })
    it('Sorting', function () {
        cy.contains('Sorting').click({force: true})
        cy.url()
                .should('include', '/#sorting')
    })
    it('Topics and Matching', function () {
        cy.contains('Topics and Matching').click({force: true})
        cy.url()
                .should('include', '/#topics-and-matching')
    })
    it('Data Sources', function () {
        cy.contains('Data Sources').click({force: true})
        cy.url()
                .should('include', '/#data-sources')
    })
    it('Favourites', function () {
        cy.contains('Favourites').click({force: true})
        cy.url()
                .should('include', '/#favourites')
    })
    it('Creating a visualisation', function () {
        cy.contains('Creating a visualisation').click({force: true})
        cy.url()
                .should('include', '/#creating-a-visualisation')
    })
    it('Definitions', function () {
        cy.contains('Definitions').click({force: true})
        cy.url()
                .should('include', '/#definitions')
            
    })
    it('Data', function () {
        cy.contains('Data').click({force: true})
        cy.url()
                .should('include', '/#data')
    })
    it('Dataset', function () {
        cy.contains('Dataset').click({force: true})
        cy.url()
                .should('include', '/#dataset')
    })
    it('Statistics', function () {
        cy.contains('Statistics').click({force: true})
        cy.url()
                .should('include', '/#statistics')
    })
    it('Open Data', function () {
        cy.contains('Open Data').click({force: true})
        cy.url()
                .should('include', '/#open-data')
    })
    it('Tags', function () {
        cy.contains('Tags').click({force: true})
        cy.url()
                .should('include', '/#tags')
    })
    it('Metadata', function () {
        cy.contains('Metadata').click({force: true})
        cy.url()
                .should('include', '/#metadata')
    })
    it('Format', function () {
        cy.contains('Format').click({force: true})
        cy.url()
                .should('include', '/#format')
    })
    it('Compliance', function () {
        cy.contains('Compliance').click({force: true})
        cy.url()
                .should('include', '/#compliance')
    })
    it('Licensing', function () {
        cy.contains('Licensing').click({force: true})
        cy.url()
                .should('include', '/#licensing')
    })
    it('GDPR', function () {
        cy.contains('GDPR').click({force: true})
        cy.url()
                .should('include', '/#gdpr')
    })
    it('Development', function () {
        cy.contains('Development').click({force: true})
        cy.url()
                .should('include', '/#development')
    })
    it('Infrastructure', function () {
        cy.contains('Infrastructure').click({force: true})
        cy.url()
                .should('include', '/#infrastructure')
    })
    it('Application Architecture', function () {
        cy.contains('Application Architecture').click({force: true})
        cy.url()
                .should('include', '/#application-architecture')
    })
    it('CLI', function () {
        cy.contains('CLI').click({force: true})
        cy.url()
                .should('include', '/#cli')
    })

})

describe('Creating a visualisation', function () {
    it('Open page', function () {
        cy.visit('https://kb.thedatatimes.com/#creating-a-visualisation')           
    })
    it('datasets', function () {
        cy.contains('dataset').click({force: true})
        cy.url()
                .should('include', '/#dataset')
    })
    it('open', function () {
        cy.contains('open').click({force: true})
        cy.url()
                .should('include', '/#open-data')
    })
    it('The Data Times', function () {
        cy.contains('The Data Times').click({force: true})
        cy.url()
                .should('include', '/#the-data-times')
    })
}) 



