describe('Test Crime Page ', function () {
    before(function () {
        cy.visit('https://dev.thedatatimes.com')           
    })
    it('menu', function () {
        cy.contains('How it works').click()   
         
        cy.contains('Data Times').click({force: true})
            cy.url()
                .should('include', '/')
            cy.visit('https://dev.thedatatimes.com/searchresults?cats=Crime')           

        cy.contains('About').click({force: true})
            cy.url()
                .should('include', '/about')
            cy.visit('https://dev.thedatatimes.com/searchresults?cats=Crime')           
        
    cy.contains('Support').click({force: true})
        cy.url()
            .should('include', '/#the-data-times')
        cy.visit('https://dev.thedatatimes.com/searchresults?cats=Crime')           

        cy.contains('Contact')

        cy.contains('0.2')
    })
    it('Input-bar', function () {
        cy.get('[type="text"]').clear()
    })
    it('Search Data Times', function () {
        cy.contains('Search Data Times').click({force: true})
        cy.visit('https://dev.thedatatimes.com/searchresults?cats=Crime')
    })
    it('Check-box', function () {
        cy.get('[type="checkbox"]').check()
        cy.get('[type="checkbox"]').uncheck()
        cy.visit('https://dev.thedatatimes.com/searchresults?cats=Crime')
    })
    it('Select-menu', function () {
        cy.get('select').select('Descending') 
    })
})



describe('Fire ', function () {
    it('Fire statistics', function () {
        cy.contains('Fire statistics: Fatalities and casualties').click({force: true})
        cy.url()
            .should('include', '/dataset/b1KvO20B-jQaiMLCG_MW')
            cy.visit('https://dev.thedatatimes.com/searchresults?cats=Crime')
   
        cy.contains('operations').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=operations')
            cy.visit('https://dev.thedatatimes.com/searchresults?cats=Crime')

        cy.contains('hospitals').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=hospitals')
            cy.visit('https://dev.thedatatimes.com/searchresults?cats=Crime')
    
        cy.contains('doctors').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=doctors')
            cy.visit('https://dev.thedatatimes.com/searchresults?cats=Crime')
   
        cy.contains('health').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=health')
            cy.visit('https://dev.thedatatimes.com/searchresults?cats=Crime')
  
        cy.contains('domestic violence').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=domestic%20violence')
            cy.visit('https://dev.thedatatimes.com/searchresults?cats=Crime')
    
        cy.contains('gps').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=gps')
            cy.visit('https://dev.thedatatimes.com/searchresults?cats=Crime')
  
        cy.contains('crime').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=crime')
            cy.visit('https://dev.thedatatimes.com/searchresults?cats=Crime')
  
        cy.contains('waiting times').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=waiting%20times')
            cy.visit('https://dev.thedatatimes.com/searchresults?cats=Crime')
        cy.contains('Explore Data').click({force: true})
        cy.url()
            .should('include', '/dataset/b1KvO20B-jQaiMLCG_MW')
            cy.visit('https://dev.thedatatimes.com/searchresults?cats=Crime')
    })
})



describe('Guatemala ', function () { 
    it('Guatemala', function () {
        cy.contains('Guatemala - Who is Doing What Where (3W)').click({force: true})
        cy.url()
            .should('include', '/dataset/R52PX20BdeI7J_hLmLJr')
            cy.visit('https://dev.thedatatimes.com/searchresults?cats=Crime')

        cy.contains('waiting times').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=waiting%20times')
            cy.visit('https://dev.thedatatimes.com/searchresults?cats=Crime')

        only.cy.contains('doctors').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=doctors')
            cy.visit('https://dev.thedatatimes.com/searchresults?cats=Crime')

        cy.contains('brexit').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=brexit')
            cy.visit('https://dev.thedatatimes.com/searchresults?cats=Crime')
    })
})