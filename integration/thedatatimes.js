describe('Test Page', function () {
    it('Open page', function () {
        cy.visit('https://dev.thedatatimes.com')           
    })
    it('How it works', function () {
        cy.contains('How it works').click()   
    })
    it('Imagine', function () {       
        cy.contains('Data Times').click({force: true})
            cy.url()
                .should('include', '/')
            cy.visit('https://dev.thedatatimes.com')           
    })
    it('About', function () {       
        cy.contains('About').click({force: true})
            cy.url()
                .should('include', '/about')
            cy.visit('https://dev.thedatatimes.com')           
    })
    it('Support', function () {       
    cy.contains('Support').click({force: true})
        cy.url()
            .should('include', '/#the-data-times')
        cy.visit('https://dev.thedatatimes.com')           
    })
    it('Contact', function () {
        cy.contains('Contact')
    })
    it('0.2', function () {
        cy.contains('0.2')
    })
    it('Input-bar', function () {
        cy.get('[type="text"]').clear()
    })
    it('Search Data Times', function () {
        cy.contains('Search Data Times').click({force: true})
        cy.visit('https://dev.thedatatimes.com')
    })
    it('Crime', function () {       
        cy.contains('Crime').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=Crime')
            cy.visit('https://dev.thedatatimes.com')           
    })   
    it('Asylum seeker', function () {       
        cy.contains('Asylum seeker').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=Asylum%20seeker')
            cy.visit('https://dev.thedatatimes.com')           
    })  
    it('Health', function () {       
        cy.contains('Health').click({force: true})
            cy.url()
                    .should('include', '/searchresults?cats=Health')
            cy.visit('https://dev.thedatatimes.com')           
    }) 
    it('Immigration', function () {       
        cy.contains('Immigration').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=Immigration')
            cy.visit('https://dev.thedatatimes.com')           
    }) 
    it('health', function () {       
        cy.contains('health').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=health')
            cy.visit('https://dev.thedatatimes.com')           
    })
   /* it('Benefits', function () {       
        cy.contains('Benefits').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=Benefits')
            cy.visit('https://dev.thedatatimes.com')           
    }) *//////////
    it('Education', function () {       
        cy.contains('Education').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=Education')
            cy.visit('https://dev.thedatatimes.com')           
    }) 
    /* it('Recycling', function () {       
        cy.contains('Recycling').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=Recycling')
            cy.visit('https://dev.thedatatimes.com')           
    }) */
    it('Politics', function () {       
        cy.contains('Politics').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=Politics')
            cy.visit('https://dev.thedatatimes.com')           
    })
    it('mountain', function () {       
        cy.contains('mountain').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=mountain')
            cy.visit('https://dev.thedatatimes.com')           
    })
    it('waiting times', function () {       
        cy.contains('waiting times').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=waiting%20times')
            cy.visit('https://dev.thedatatimes.com')           
    })
})

