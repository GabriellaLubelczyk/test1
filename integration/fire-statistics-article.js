describe('Test Fire statistics page ', function () {
    beforeEach(function () {
        cy.visit('https://dev.thedatatimes.com/dataset/b1KvO20B-jQaiMLCG_MW')           
    })
    it('How it works', function () {
        cy.contains('How it works').click({force: true})   
    })   
    it('Imagine', function () {       
        cy.contains('Data Times').click({force: true})
            cy.url()
                .should('include', '/')           
    })
    it('About', function () {       
        cy.contains('About').click({force: true})
            cy.url()
                .should('include', '/about')           
    })
    it('Support', function () {       
    cy.contains('Support').click({force: true})
        cy.url()
            .should('include', '/#the-data-times')           
    })
    it('Contact', function () {
        cy.contains('Contact')
    })
    it('0.2', function () {
        cy.contains('0.2')
    })
    it('Input-bar', function () {
        cy.get('[type="text"]').clear()
    })
    it('Search Data Times', function () {
        cy.contains('Search Data Times').click({force: true}) 
    }) 
    it('?', function () {
        cy.get('[role = "img"]')
    })
    it('Back to Search Results', function () {
        cy.contains('Back to Search Results').click({force: true}) 
        cy.url()
    })
    /*
    describe('Topics', function () {
        it('operations', function () {
            cy.contains('operations').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=operations')
        })
        it('hospitals', function () {
            cy.contains('hospitals').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=hospitals')
        })
        it('doctors', function () {
            cy.contains('doctors').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=doctors')
        }) 
        it('health', function () {
            cy.contains('health').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=health')
        })
        it('domestic violence', function () {
            cy.contains('domestic violence').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=domestic%20violence')
        })
        it('gps', function () {
            cy.contains('gps').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=gps')
        })
        it('crime', function () {
            cy.contains('crime').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=crime')
        })
        it('waiting times', function () {
            cy.contains('waiting times').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=waiting%20times')
        })
    }) */

    it('lintol', function () {
        cy.contains('Lintol')
    })
    
})